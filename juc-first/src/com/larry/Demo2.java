package com.larry;

public class Demo2 {

    /**
     * 主线程结束，用户线程还在继续,jvm存活
     * @param args
     */
    public static void main(String[] args) {
        new Thread(() -> {
            //用户线程
            System.out.println(Thread.currentThread().getName() + "---" + Thread.currentThread().isDaemon());
            while (true){

            }
        },"aa").start();

        System.out.println(Thread.currentThread().getName() + "主线程结束");
    }

}
