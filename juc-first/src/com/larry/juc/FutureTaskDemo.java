package com.larry.juc;

import java.util.concurrent.*;

/**
 * @ProjectName JUC
 * @Author larry
 * @Date 20:44 2021/7/12
 * @Description TODO
 **/
public class FutureTaskDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        FutureTask<Integer> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + "---" + "执行");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
        });
        new Thread(futureTask,"T1").start();

        //不见不散  只要出现get方法，不管是否计算完成都会阻塞
//        System.out.println(futureTask.get());

        //过时不候   超时时间
//        System.out.println(futureTask.get(2L,TimeUnit.SECONDS));


        //用轮询替代阻塞
        while (true){
            if (futureTask.isDone()){
                System.out.println("" + futureTask.get());
                break;
            }else {
                System.out.println("执行中");
            }
        }

    }
}
