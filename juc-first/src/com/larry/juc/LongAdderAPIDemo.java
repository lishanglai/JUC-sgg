package com.larry.juc;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;

/**
 * @ProjectName JUC
 * @Author larry
 * @Date 16:26 2021/7/19
 * @Description LongAdder只能用来计算加法，且从零开始计算
 *              LongAccumulator提供了自定义的函数操作
 **/
public class LongAdderAPIDemo {

    public static void main(String[] args) {
        LongAdder longAdder = new LongAdder();
        longAdder.increment();
        longAdder.increment();
        longAdder.increment();
        System.out.println(longAdder.longValue());

        /**
         * @FunctionalInterface
         * public interface LongBinaryOperator {
         *     long applyAsLong(long var1, long var3);
         * }
         */
        LongAccumulator longAccumulator = new LongAccumulator((x, y) -> x + y, 0);
        longAccumulator.accumulate(1);
        longAccumulator.accumulate(2);
        longAccumulator.accumulate(3);

        System.out.println(longAccumulator.longValue());
    }

}
