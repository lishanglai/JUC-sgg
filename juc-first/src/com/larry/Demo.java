package com.larry;

public class Demo {
    public static void main(String[] args) {
        Thread thread = new Thread();
        /**
         * 线程状态
         * public static enum State {
         *         NEW,     新建
         *         RUNNABLE,    准备就绪
         *         BLOCKED,     阻塞
         *         WAITING,     等待（不见不散）
         *         TIMED_WAITING,       等待（过时不候）
         *         TERMINATED;      终结
         *
         *         private State() {
         *         }
         *     }
         */
        thread.getState();


        /**
         * wait和sleep区别
         *  sleep是Thread的静态方法，wait是Object的方法，任何对象实例都可以调用
         *  sleep不会释放锁，也不需要占用锁。wait会释放锁，但调用它的前提是当前线程占有锁（即代码要在synchronized中）
         *  他们都可以被interrupted方法中断
         */
    }
}
