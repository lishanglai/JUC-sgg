package com.larry;

import org.openjdk.jol.info.ClassLayout;

/**
 * @ProjectName JUC
 * @Author larry
 * @Date 13:09 2021/7/22
 * @Description TODO
 **/
public class ObjectHeadDemo {
    public static void main(String[] args) {
        ////jvm的细节详细情况
        //System.out.println(VM.current().details());
        //
        ////所有对象分配的字节都是8的整数倍
        //System.out.println(VM.current().objectAlignment());

        Object object = new Object();
        //引入了JOL，直接使用
        //System.out.println(ClassLayout.parseInstance(object).toPrintable());
        System.out.println(ClassLayout.parseInstance(new MyObject()).toPrintable());
    }
}

class MyObject{
    int i = 25;
    boolean flag = false;
}